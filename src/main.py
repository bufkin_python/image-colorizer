#! /usr/bin/env/python311

import numpy as np
from numpy import full, concatenate, newaxis, load, float32
from cv2 import dnn, imread, resize, cvtColor

if __name__ == '__main__':
    prototxt_path = 'models/colorization_deploy_v2.prototxt'
    model_path = 'models/colorization_release_v2.caffemodel'
    kernel_path = 'models/pts_in_hull.npy'
    image_path = 'lion.jpg'

    net = dnn.readNetFromCaffe(prototxt_path, model_path)
    points = load(kernel_path)

    points = points.transpose().reshape(2, 313, 1, 1)
    net.getLayer(net.getLayerId("class8_ab")).blobs = [
        points.astype(float32)]
    net.getLayerId("conv8_313_rh").blobs = [
        full([1, 313], 2.606, dtype="float32")]

    bw_image = imread(image_path)
    normalized = bw_image.astype("float32") / 255.0
    lab = cvtColor(normalized, COLOR_BGR2LAB)

    resized = resize(lab, (224, 224))
    L = split(resized)[0]
    L -= 50

    net.setInput(dnn.blobFromImage(L))
    ab = net.forward()[0, :, :, :].transpose((1, 2, 0))

    ab = resize(ab, (bw_image.shape[1], bw_image.shape[0]))
    L = split(lab)[0]

    colorized = concatenate((L[:, :, newaxis], ab), axis=2)
    colorized = cvtColor(colorized, COLOR_LAB2BGR)
    colorized = (255.0*colorized).astype("uint8")

    imshow("BW Image", bw_image)
    imshow("Colorized", colorized)
    waitKey(0)
    destroyAllWindows()
